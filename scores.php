<?php

$file = "/tmp/scores.txt";

$postData = file_get_contents('php://input');
if ($postData != null) {
    $json = json_decode($postData, true);
    file_put_contents($file, serialize($json));
    echo json_encode($json);
}

if (!file_exists($file)) {
    file_put_contents($file, serialize(array("scores" => array(0,0,0), 
                                             "players" => array("Player 1", "Player 2", "Player 3"))));
}

$data = unserialize(file_get_contents($file));
echo json_encode($data);

