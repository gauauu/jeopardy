# Jeopardy #

Some relatively simple tools for hosting a Jeopardy game. These were used for a one-off game at a business meeting at my church, and are not tested, bug-free, the slightest bit secure, or anything like that. I don't recommend hosting them publicly on the web -- this is for setting up a relatively nice-looking in-person jeopardy game. It has:

* A board with customizable questions and answers
* A backstage scorekeeping tool
* Screens for each player's name and score

### How do I get set up? ###

First, this is not very user-friendly or anything. I built it for my own use, so you'll want some programming knowledge if you are wanting to do anything much with it.

* This needs to run on a webserver with php installed. (Although it could very easily be adapted to any server-side language. I used PHP because I already had it installed.  Most of the functionality is written in javascript) It writes to a file in /tmp (so for now, the server only works in a *nix OS, but that's really easy to change), so it doesn't need mysql or any other persistence system or database set up.
* To configure the questions/answers, edit the gameData.json file. Alternatively, you can put it all in a simple text format (see questionsRaw.txt), and run importQuestions.php from the command line to generate gameData.json from your questions (this makes it easier for non-technical folks to write questions).  The tool assumes you will have 5 categories per game. You may specify more than one game in your gameData, and the board will let you select which one to use. (To specify multiple games in the questionsRaw.txt, just continue making new categories. Every 5 categories will be a new game)
* The scorekeeper runs controller.html. You can modify the names of players, and use the on-screen buttons to control player scores.
* For each players' score screen, point a browser to player.php, with a parameter of the player's id, 0-2 (ie player.php?player=0)

alkdsjflkajs

more changes
