<?php

    ob_start();
    include "scores.php";
    ob_end_clean();
    $playerNum = @$_GET['player'];
    if ($playerNum == null || !in_array($playerNum, array(0,1,2))) {
        echo "Player must be 0, 1, 2";
        exit;
    }
    $score = $data['scores'][$playerNum];
    $name = @$data['players'][$playerNum];
    if ($name == null) {
        $name = "Player $playerNum";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="1" >
    </head>
    <body style='background-color: #060CE9; color: white; text-align: center'>
        <div style='font-size: 30vw; font-weight: bold; font-family: sans-serif; text-shadow: 15px 15px 15px #000; margin-top:30px'>$<?=$score?></div>
        <div style='font-size: 20vw; font-weight: bold; font-family: noteworthy; margin-top: -80px'><?=$name?></div>
    </body>
</html>


