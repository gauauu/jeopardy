<?php

$lines = file('questionsRaw.txt');

$gameData = array();
$game = array();
$category = array();
$q = array();
$categoryLine = 0;
$categoryCtr = 0;
foreach ($lines as $line) {
    $line = trim($line);
    if (trim($line) == '') {
        continue;
    }

    if ($categoryLine == 0) {
        $category = array(
            'name' => $line,
            'qs' => array()
        );
        $categoryLine++;
        continue;
    }

    if (($categoryLine % 2) == 1) {
        $q = array(
            'q' => $line
        );
    } else {
        $q['a'] = $line;
        $category['qs'][] = $q;
    }

    $categoryLine++;
    if ($categoryLine == 11) {
        $game[] = $category;
        $categoryLine = 0;
        $categoryCtr++;
        if ($categoryCtr == 5) {
            $gameData[] = $game;
            $game = array();
            $categoryCtr = 0;
        }
    }
}


file_put_contents('gameData.json', json_encode(array("games" => $gameData)));
